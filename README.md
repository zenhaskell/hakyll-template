# hakyll-template

This is a Hakyll template that is automatically deployed to [Gitlab
pages](https://zenhaskell.gitlab.io/hakyll-template). To use it simply fork the
repository on gitlab and add your own content. Gitlab's CI will take care of
the rest.
