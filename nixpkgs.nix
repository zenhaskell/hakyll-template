import (builtins.fetchTarball {
  name = "nixos-unstable-2019-03-07-5b4152a911f61b2a6e23d668f6f2f3abe4b79c0b";
  url = "https://github.com/nixos/nixpkgs/archive/5b4152a911f61b2a6e23d668f6f2f3abe4b79c0b.tar.gz";
  sha256 = "0jaaddmjpbf9y0ddj3nvcp1wd4zbjhmbd23wj10z3nlfygsnipil";
})
