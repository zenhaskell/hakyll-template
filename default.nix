{ pkgs ? import ./nixpkgs.nix {}, ... }:
let
   f = import (./stack2nix.nix) { pkgs = pkgs; };
in
with f;
hakyll-template
